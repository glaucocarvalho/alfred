from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from dropbox_app import utils
import pprint


def index(request):
    if request.method != 'POST':
        if request.user.is_authenticated:
            folder_contents = utils.dropbox_list_folder(request.user, '')
        else:
            folder_contents = None
        context = {
            "path": '/',
            "folder_contents": folder_contents,
        }
        return render(request, "index.html", context)
    elif request.method == 'POST':
        data = request.POST.copy()
        if request.user.is_authenticated:
            folder_contents = utils.dropbox_list_folder(request.user, data['path'])
        else:
            folder_contents = None
        context = {
            "path": data['path'],
            "folder_contents": folder_contents,
        }
        return render(request, "index.html", context)


def get_content(user, path):
    return list(utils.dropbox_list_folder(user, path))


def get_content_list(user, folder_contents, sub_folders):
    content_list = []

    for file in folder_contents:
        if file['tag'] == 'file':
            content_list.append(file['content_hash'])
        elif file['tag'] == 'folder' and sub_folders == 1:
            folder_content = get_content(user, file['path_display'])
            sub_folder_content_list = get_content_list(user, folder_content, sub_folders)
            for s in sub_folder_content_list:
                content_list.append(s)

    return content_list


def get_redun_list(user, folder_contents, sub_folders, content_list):
    redun_list = []

    for file in folder_contents:
        if file['tag'] == 'file':
            c = content_list.count(file['content_hash'])
            if c > 1:
                redun_list.append([file, 'conteudo repetido'])
        elif file['tag'] == 'folder' and sub_folders == 1:
            folder_content = get_content(user, file['path_display'])
            sub_folder_redun_list = get_redun_list(user, folder_content, sub_folders, content_list)

            for s in sub_folder_redun_list:
                redun_list.append(s)

    return redun_list


def check_content(user, folder_contents, sub_folders, base=0):
    content_list = get_content_list(user, folder_contents, sub_folders)
    redun_list = get_redun_list(user, folder_contents, sub_folders, content_list)
    redun_dict = {}

    for r in redun_list:
        if r[1] == 'conteudo repetido':
            key = r[0]['content_hash']
            if key not in redun_dict:
                redun_dict[key] = {}
                redun_dict[key]['type'] = 'conteudo repetido'
                redun_dict[key]['files'] = []
            if key in redun_dict:
                name = ''
                if sub_folders == 0:
                    name = r[0]['name']
                elif sub_folders == 1:
                    name = r[0]['path_display']
                redun_dict[key]['files'].append(name)
    return redun_list, redun_dict


def redundancy(request, sub_folders):
    data = request.POST.copy()
    if request.user.is_authenticated:
        folder_contents = list(utils.dropbox_list_folder(request.user, data['path']))
    else:
        folder_contents = None

    redun_list, redun_dict = check_content(request.user, folder_contents, sub_folders)

    context = {
        "path": data['path'],
        "folder_contents": folder_contents,
        "redundancies": redun_list,
        "redict": redun_dict,
    }
    return render(request, "redundancy.html", context)


def get_folder(request, path):
    if request.user.is_authenticated:
        folder_contents = utils.get_folder_content(request.user, path)
    else:
        folder_contents = None
    context = {
        "folder_contents": folder_contents,
    }
    # pprint.pprint(context)
    return render(request, 'index.html', context)


def logout(request):
    return render(request, 'logout.html')

from django.urls import path, re_path
from . import views


urlpatterns = [
    path('', views.index, name='index'),
    path('red/<int:sub_folders>/', views.redundancy, name='redundancias'),
    path('logout/', views.logout, name='logout')
    # path('dropbox/path/', views.get_folder, name='get_folder'),
    ]

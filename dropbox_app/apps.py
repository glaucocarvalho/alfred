from django.apps import AppConfig


class DropboxAppConfig(AppConfig):
    name = 'dropbox_app'

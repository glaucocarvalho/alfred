import dropbox
import requests
import pprint


def undotted_keys(dict):
    """
    Transform dotted keys to undotted keys. This is for the `.tag` key
    in the dict that Dropbox returns.
    """
    return {k.lstrip("."): v for k, v in dict.items()}


def get_folder_content(user, path):
    social_auth = user.social_auth.filter(provider="dropbox-oauth2").get()
    dbx = dropbox.Dropbox(social_auth.access_token)
    msg = []
    for entry in dbx.files_list_folder(path).entries:
        # if hasattr(entry, 'content_hash'):  # file
        msg.append(entry.path_display)
        if not hasattr(entry, 'content_hash'):  #folder
            msg = msg + (get_folder_content(user, entry.path_display))
    return (msg)


def dropbox_list_folder(user, path):
    """
    Returns a generator of files and subfolders for the given folder.
    """
    if path == '/':
        path = ''
    social_auth = user.social_auth.filter(provider="dropbox-oauth2").get()
    session = requests.Session()
    session.headers.update({
        "Authorization": "Bearer {token}".format(token=social_auth.access_token),
        "Content-Type": "application/json",
    })
    response = session.post(
        url="https://api.dropboxapi.com/2/files/list_folder",
        json={"path": path},
    )
    response.raise_for_status()
    content = response.json()
    for entry in content['entries']:
        yield undotted_keys(entry)

    while content['has_more']:
        response = session.post(
            url="https://api.dropboxapi.com/2/files/list_folder/continue",
            json={"cursor": content["cursor"]},
        )
        response.raise_for_status()
        content = response.json()
        for entry in content['entries']:
            yield undotted_keys(entry)
